import './App.css';
import Home from './components/Home/Home';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import CreatePost from './components/CreatePost/CreatePost';
import { Navbar, Nav } from 'react-bootstrap';
import Post from './components/Post/Post';

function App() {

  return (
    <div className="App">
      <Router>
        {/* <Link to="/">Home</Link> <br />
        <Link to="/createpost">Create a Post</Link> */}
        <Navbar bg="dark" variant="dark">
          <Navbar.Brand href="/">Technext</Navbar.Brand>
          <Nav className="mr-auto">
            <Nav.Link href="/">Home</Nav.Link>
            <Nav.Link href="/createpost">Create Post</Nav.Link>
            {/* <Nav.Link href="#pricing">Pricing</Nav.Link> */}
          </Nav>
        </Navbar>
        <Switch>
          <Route path="/" exact>
            <Home></Home>
          </Route>
          <Route path="/createpost" exact>
            <CreatePost></CreatePost>
          </Route>
          <Route path="/post/:id" exact>
            <Post></Post>
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
