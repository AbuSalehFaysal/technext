import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';

const Post = () => {
    let { id } = useParams();
    const [postObject, setPostObject] = useState({});
    useEffect(() => {
        fetch(`http://localhost:5000/posts/byId/${id}`)
            .then(res => res.json())
            .then(data => {
                setPostObject(data);
            })
    })
    return (
        // <div>
        //     <h1>{postObject.title}</h1>
        //     <h1>{postObject.postText}</h1>
        //     <h1>{postObject.username}</h1>
        //     <h1>{(new Date(postObject.createdAt).toDateString('dd/MM/yyyy'))}</h1>
        //     <h1>{(new Date(postObject.updatedAt).toDateString('dd/MM/yyyy'))}</h1>
        // </div>
        <div className="container">
            <div className="row">
                <div className="col-md-6">
                    <div className="card">
                        <div className="card-header">
                            Post Id: {id}
                        </div>
                        <div className="card-body">
                            <h5 className="card-title">{postObject.title}</h5>
                            <p className="card-text">{postObject.postText}</p>
                        </div>
                        <div className="card-footer">
                            <p>Created By: {postObject.username}</p>
                            <p>Created At: {(new Date(postObject.createdAt).toDateString('dd/MM/yyyy'))}</p>
                            <p>Updated At: {(new Date(postObject.createdAt).toDateString('dd/MM/yyyy'))}</p>
                            {/* <p>Created By: {postObject.usename}</p> */}
                        </div>
                    </div>
                </div>
                <div className="col-md-6">
                    <h1>Comment Section</h1>
                </div>
            </div>
        </div>
    );
};

export default Post;