import React from 'react';
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import './CreatePost.css';
import axios from 'axios';

const CreatePost = () => {

    const initialValues = {
        title: "",
        postText: "",
        username: ""
    }

    const onSubmit = (data) => {
        // console.log(data);
        axios.post("http://localhost:5000/posts", data)
        .then( (res) => {
            console.log("DATA INSERTED");
        });
    }

    const validationSchema = Yup.object().shape({
        title: Yup.string().required("You must input a title"),
        postText: Yup.string().required("You must input a description"),
        username: Yup.string().min(3).max(100).required(),
    });
    
    return (
        <div>
            <Formik initialValues={initialValues} onSubmit={onSubmit} validationSchema={validationSchema}>
                <Form>
                    <label htmlFor="">Title : </label>
                    <ErrorMessage name="title"component="span"></ErrorMessage>
                    <Field id="" name="title" placeholder="Ex. Title ..."></Field>
                    <br/>
                    <br/>
                    <label htmlFor="">Description : </label>
                    <ErrorMessage name="postText"component="span"></ErrorMessage>
                    <Field id="" name="postText" placeholder="Ex. Description ..."></Field>
                    <br/>
                    <br/>
                    <label htmlFor="">Username : </label>
                    <ErrorMessage name="username"component="span"></ErrorMessage>
                    <Field id="" name="username" placeholder="Ex. Abu Saleh Faysal ..."></Field>
                    <br/>
                    <br/>
                    <button type="submit">Create Post</button>
                </Form>
            </Formik>
        </div>
    );
};

export default CreatePost;