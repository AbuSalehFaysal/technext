import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import PostList from '../PostList/PostList';

const Home = () => {

    const [posts, setPosts] = useState([]);

    let history = useHistory();

    useEffect(() => {
        const url = `http://localhost:5000/posts`;
        fetch(url)
            .then(res => res.json())
            .then(data => setPosts(data))
    }, [])

    return (
        <div className="App">
            <br/>
            {
                posts.map(post => <PostList key={post.id} post={post}></PostList>)
            }
        </div>
    );
};

export default Home;