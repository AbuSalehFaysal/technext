import React from 'react';
import { useHistory } from 'react-router';

const PostList = (props) => {
    const { id, title, postText, username, createdAt, updatedAt } = props.post;
    let history = useHistory();
    return (
        <div className="container">
            <div className="card">
                <div className="card-header">
                    Post Id: {id}
                </div>
                <div className="card-body">
                    <h5 className="card-title">{title}</h5>
                    <p className="card-text">{postText}</p>
                    <a onClick={ () => {history.push(`/post/${id}`)} } class="btn btn-primary">View Post</a>
                </div>
            </div>
        </div>
    );
};

export default PostList;