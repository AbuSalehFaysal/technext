const express = require("express");
const app = express();
const port = 5000;
const db = require('./models');
const cors = require("cors");

app.use(express.json());
app.use(cors());

// ROUTERS 
const postRouter = require('./routes/Posts')
app.use("/posts", postRouter);

const commentsRouter = require('./routes/Comments')
app.use("/comments", postRouter);

db.sequelize.sync().then(()=>{
    app.listen(port, () => {
        console.log("SERVER HAS STARTED!");
    });
});
